package springboot.project.ecosystem.jdbc.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import springboot.project.ecosystem.jdbc.template.service_impl.ProducerService;

@SpringBootApplication
public class JdbctemplateApplication {

    //update Test = Dùng ApplicationContext thay cho lib SpringBootTest
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(JdbctemplateApplication.class, args);
        loadAllProducer(applicationContext);
    }

    private static void loadAllProducer(ApplicationContext applicationContext) {
        ProducerService producerService = applicationContext.getBean(ProducerService.class);
        findAllProducts(producerService);
    }

    private static void findAllProducts(ProducerService producerService) {
        producerService.loadAllProducer();
    }

}
