package springboot.project.ecosystem.jdbc.template.service;

import springboot.project.ecosystem.jdbc.template.entity.Producer;

import java.util.List;

public interface IProducerService {
    void addProducer(Producer producer) throws Exception;

    List<Producer> loadAllProducer() throws Exception;
}
