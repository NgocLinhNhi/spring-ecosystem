package springboot.project.ecosystem.jdbc.template.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

@Component
public class DBConfig {

    private static JdbcTemplate jdbcTemplate;
    private static NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    //Dùng Bean component =>autowired không cần khởi tạo singleton nữa => call Autowired mới lấy được bean DataSource
//    private static DBConfig INSTANCE;
//    public static DBConfig getInstance() {
//        if (INSTANCE == null) INSTANCE = new DBConfig();
//        return INSTANCE;
//    }

    //DataSource => Auto load config dataSource trong file application.properties
    @Autowired
    private DataSource dataSource;

    public JdbcTemplate getConnection() {
        if (jdbcTemplate == null)
            jdbcTemplate = new JdbcTemplate(dataSource);

        return jdbcTemplate;
    }

    public NamedParameterJdbcTemplate getNamedConnection() {
        if (namedParameterJdbcTemplate == null)
            namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        return namedParameterJdbcTemplate;
    }
}
