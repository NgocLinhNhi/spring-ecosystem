package springboot.project.ecosystem.jdbc.template.sql;

public class SqlQuerry {
    public static final String PRODUCER_LOAD_ALL = "SELECT * FROM producer";

    public static StringBuilder insertProducer() {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO PRODUCER                 ")
                .append("    (SEQ_NO,                    ")
                .append("    PRODUCER_NAME,              ")
                .append("    SYS_STATUS)                 ")
                .append("    VALUES(?,?,?)               ");
        return sql;
    }
}
