package springboot.project.ecosystem.jdbc.template.service_impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import springboot.project.ecosystem.jdbc.template.config.DBConfig;
import springboot.project.ecosystem.jdbc.template.entity.Producer;
import springboot.project.ecosystem.jdbc.template.object_mapper.ProducerMapper;
import springboot.project.ecosystem.jdbc.template.service.IProducerService;

import java.util.List;

import static springboot.project.ecosystem.jdbc.template.sql.SqlQuerry.PRODUCER_LOAD_ALL;
import static springboot.project.ecosystem.jdbc.template.sql.SqlQuerry.insertProducer;

@Repository
public class ProducerService implements IProducerService {
    private Logger logger = LoggerFactory.getLogger(ProducerService.class);

    @Autowired
    DBConfig dbConfig;

    @Override
    public void addProducer(Producer producer) {
        JdbcTemplate jdbcTemplate = dbConfig.getConnection();
        jdbcTemplate.update(
                insertProducer().toString(),
                producer.getSeqNo(),
                producer.getProducerName(),
                producer.getSysStatus());
        logger.info("Add New producer has successfully !!!!");
    }

    @Override
    public List<Producer> loadAllProducer() {
        JdbcTemplate jdbcTemplate = dbConfig.getConnection();
        List<Producer> lstProducer = jdbcTemplate.query(PRODUCER_LOAD_ALL, new ProducerMapper());
        for (Producer producer : lstProducer) {
            logger.info("Producer Name : {}", producer.getProducerName());
        }
        return lstProducer;
    }

}
