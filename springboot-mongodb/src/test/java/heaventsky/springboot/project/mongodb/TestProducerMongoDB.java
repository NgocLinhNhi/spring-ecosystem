package heaventsky.springboot.project.mongodb;

import heaventsky.springboot.project.mongodb.entity.Producer;
import heaventsky.springboot.project.mongodb.serviceimpl.ProducerServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestProducerMongoDB {

    private Logger logger = LoggerFactory.getLogger(TestProducerMongoDB.class);

    @Autowired
    private ProducerServiceImpl producerService;

    @Test
    public void findAllProducer() {
        List<Producer> loadAllProducer = producerService.loadAllProducer();
        logger.info("================= MONGO REPOSITORY ===================");
        for (Producer producer : loadAllProducer) {
            logger.info("Producer Name : {}", producer.getProducerName());
            logger.info("SysStatus : {}", producer.getSysStatus());
        }
    }

    @Test
    public void findAllProducerByJdbcTemolateMongo(){
        List<Producer> loadAllProducer = producerService.loadAllProducerMongoTemplate();
        logger.info("================= MONGO JDBC TEMPLATE ===================");
        for (Producer producer : loadAllProducer) {
            logger.info("Producer Name : {}", producer.getProducerName());
        }
    }

}
