package heaventsky.springboot.project.mongodb.serviceimpl;

import heaventsky.springboot.project.mongodb.entity.Producer;
import heaventsky.springboot.project.mongodb.repository.ProducerRepositoryMongo;
import heaventsky.springboot.project.mongodb.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProducerServiceImpl implements ProducerService {

    // sử dụng MongoRepository
    @Autowired
    ProducerRepositoryMongo producerRepositoryMongo;

    // xử dụng mongoTemplate
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<Producer> loadAllProducer() {
        return producerRepositoryMongo.findAll();
    }

    @Override
    public List<Producer> loadAllProducerMongoTemplate() {
        return mongoTemplate.findAll(Producer.class);
    }

}
