package heaventsky.springboot.project.mongodb.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Document(collection = "producer")
@Getter
@Setter
public class Producer implements Serializable {

    private static final long serialVersionUID = -4988499961184910623L;

    @Id
    private String seq_no;

    @Field(value = "producer_name")
    private String producerName;

    @Field(value = "sysStatus")
    private String sysStatus;

    public Producer() {
        super();
    }

    public Producer(String seq_no, String producerName, String sysStatus) {
        super();
        this.seq_no = seq_no;
        this.producerName = producerName;
        this.sysStatus = sysStatus;
    }

}
