package heaventsky.springboot.project.mongodb.service;

import heaventsky.springboot.project.mongodb.entity.Producer;

import java.util.List;

public interface ProducerService {
    List<Producer> loadAllProducer();

    List<Producer> loadAllProducerMongoTemplate();
}
