package heaventsky.springboot.project.mongodb;

import heaventsky.springboot.project.mongodb.entity.Producer;
import heaventsky.springboot.project.mongodb.serviceimpl.ProducerServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.List;

@SpringBootApplication
public class MongodbApplication {
    private static Logger logger = LoggerFactory.getLogger(MongodbApplication.class);

    //Tạo ApplicationContext để getBean test thay cho SpringTest
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(MongodbApplication.class, args);
        getBean(applicationContext);
    }

    private static void getBean(ApplicationContext applicationContext) {
        ProducerServiceImpl producerService = applicationContext.getBean(ProducerServiceImpl.class);
        loadAllProducer(producerService);
    }

    private static void loadAllProducer(ProducerServiceImpl producerService) {
        List<Producer> loadAllProducer = producerService.loadAllProducer();
        logger.info("====================================");
        for (Producer producer : loadAllProducer) {
            logger.info("Producer Name : {}", producer.getProducerName());
            logger.info("SysStatus : {}", producer.getSysStatus());
        }
    }
}
