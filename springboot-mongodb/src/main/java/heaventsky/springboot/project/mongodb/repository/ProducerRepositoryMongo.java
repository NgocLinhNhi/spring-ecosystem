package heaventsky.springboot.project.mongodb.repository;

import heaventsky.springboot.project.mongodb.entity.Producer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProducerRepositoryMongo extends MongoRepository<Producer, String> {

}
