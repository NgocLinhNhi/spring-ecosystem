package springboot.project.ecosystem.mybatis;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.project.ecosystem.mybatis.domain.ProductView;
import springboot.project.ecosystem.mybatis.service_impl.ProductServiceAnnotationImpl;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestSpringBootMyBatis {

    private Logger logger = LoggerFactory.getLogger(TestSpringBootMyBatis.class);

    @Autowired
    private ProductServiceAnnotationImpl productServiceAnnotationImpl;

    @Test
    public void loadAllProductsAnnotation() {
        List<ProductView> productViews = productServiceAnnotationImpl.loadAllProductAnnotation();

        logger.info("================= LOAD BY Annotation ================= ");
        for (ProductView product : productViews) {
            logger.info("Product Name : {} Category Name : {}", product.getProductName(), product.getCategoryId());
        }
    }

}
