package springboot.project.ecosystem.mybatis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import springboot.project.ecosystem.mybatis.domain.ProductView;
import springboot.project.ecosystem.mybatis.service_impl.ProductServiceAnnotationImpl;

import java.util.List;

@SpringBootApplication
public class MybatisApplication {
    private static Logger logger = LoggerFactory.getLogger(MybatisApplication.class);

    //Test dùng ApplicationContext để gọi Bean không phụ thuộc SpringTesst
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(MybatisApplication.class, args);
        loadProductTest(applicationContext);
    }

    private static void loadProductTest(ApplicationContext applicationContext) {
        ProductServiceAnnotationImpl productServiceImpl = applicationContext.getBean(ProductServiceAnnotationImpl.class);
        findAllProducts(productServiceImpl);

    }

    public static void findAllProducts(ProductServiceAnnotationImpl productServiceImpl) {
        List<ProductView> productViews = productServiceImpl.loadAllProductAnnotation();

        logger.info("================= LOAD BY Annotation ================= ");
        for (ProductView product : productViews) {
            logger.info("Product Name : {} Category Name : {}", product.getProductName(), product.getCategoryId());
        }
    }
}
