package springboot.project.ecosystem.mybatis.service;

import springboot.project.ecosystem.mybatis.domain.ProductView;

import java.util.List;

public interface IProductServiceAnnotation {
    List<ProductView> loadAllProductAnnotation();
}
