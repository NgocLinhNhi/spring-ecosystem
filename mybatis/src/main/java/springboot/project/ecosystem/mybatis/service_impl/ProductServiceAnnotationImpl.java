package springboot.project.ecosystem.mybatis.service_impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springboot.project.ecosystem.mybatis.domain.ProductView;
import springboot.project.ecosystem.mybatis.repository.ProductDaoAnnotation;
import springboot.project.ecosystem.mybatis.service.IProductServiceAnnotation;

import java.util.List;

@Service
public class ProductServiceAnnotationImpl implements IProductServiceAnnotation {

    @Autowired
    protected ProductDaoAnnotation productDaoAnnotation;

    @Override
    public List<ProductView> loadAllProductAnnotation() {
        return productDaoAnnotation.loadAllProductAnnotation();
    }
}
