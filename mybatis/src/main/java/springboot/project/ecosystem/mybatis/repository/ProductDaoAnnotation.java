package springboot.project.ecosystem.mybatis.repository;//package springboot.project.ecosystem.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import springboot.project.ecosystem.mybatis.domain.ProductView;

import java.util.List;

@Mapper
@Repository
public interface ProductDaoAnnotation {

    //dùng annotation thay cho file .xml(sql-query) - mapper entity trả về ở trong này
    @Select(" SELECT * from PRODUCT ")
    @Results(value = {
            @Result(property = "seqPro", column = "SEQ_PRO"),
            @Result(property = "productName", column = "PRODUCT_NAME"),
            @Result(property = "categoryId", column = "CATEGORY_ID"),
            @Result(property = "price", column = "PRICE"),
            @Result(property = "imageProduct", column = "IMAGE_URL"),
            @Result(property = "numberSales", column = "NUMBER_SALES"),
            @Result(property = "guarantee", column = "GUARANTEE"),
            @Result(property = "updateDate", column = "UPDATE_DATE"),
            @Result(property = "createDate", column = "CREATE_DATE"),
            @Result(property = "sysStatus", column = "SYS_STATUS"),
    })
    List<ProductView> loadAllProductAnnotation();
}
