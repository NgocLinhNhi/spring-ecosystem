'use strict';

var app = angular.module('productManagement',['ui.router','ngStorage']);
//set tham số  ui.router để có thể dùng được chế độ singlePage state  --> ngStorage để dùng được $localStorage ở service

app.constant('urls', {
	USER_SERVICE_API : '/product/loadAllProduct',
	PRODUCT_DELETE : '/product/deleteProduct/',
	FIND_PRODUCT_BY_SEQPRO:'/product/findProductBySeq/',
	ADD_PRODUCT:'/product/addNewProduct',
	UPDATE_PRODUCT:'/product/updateProduct'
});

app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
	
		$urlRouterProvider.otherwise('/');
	
        $stateProvider 
			        /*
					 * chế độ singlePage product-home được sử dụng ở html dưới
					 * dạng này ui-sref="product-home" -> button nào kick vào có
					 * state nào thì trỏ đển state đó trong app.js
					 */
            .state('product-home', {
                url: '',
                templateUrl: 'product.html', //trả về trang html
                controller:'ProductController',
                controllerAs:'vm',
                resolve: {
                    product: function ($q, ProductService) {
                        var deferred = $q.defer();
                        //gọi đến hàm loadAllProduct để xử lý gọi đến back-end lấy list product
                        ProductService.loadAllProducts().then(deferred.resolve, deferred.resolve);
                        return deferred.promise;
                    }
                }
            });
        
    }]);

