'use strict';

//tạo 1 factory để chứa nhiều service
angular.module('productManagement').factory('ProductService',['$localStorage', '$http', '$q', 'urls',
        function ($localStorage, $http, $q, urls) {
    	//urls tham số được truyền từ bên app.js
            return {
            		//không khai báo ở factory ở đây sẽ không gọi được function bên controller 
            		loadAllProducts: loadAllProducts,
            		getAllProducts: getAllProducts,
            		deleteProduct : deleteProduct,
            		findProductById: findProductById,
            		addProduct:addProduct,
            		updateProduct:updateProduct
            };

            //hàm này để lấy data list product và set vào localStorage
            function loadAllProducts() {
                var deferred = $q.defer();
                $http.get(urls.USER_SERVICE_API) //dùng constant được định nghĩa
                    .then(
                        function (response) {
                            $localStorage.product = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            deferred.reject(errResponse);
                        }
                    );
                //return $localStorage.product ;
                //nếu không trả data ở đây thì phải tạo 1 hàm để trả về giá trị data của $localStorage được set
                return deferred.promise;
            }

            // tạo 1 hàm để trả về list data product đơn thuần hoặc trả luôn ở hàm trên
            function getAllProducts(){
                return $localStorage.product;
            }
            
            function deleteProduct(id) {
                var deferred = $q.defer();
                $http.post(urls.PRODUCT_DELETE + id)
                    .then(
                        function (response) {
                        	loadAllProducts();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
            function findProductById(seqPro) {
            	var deferred = $q.defer();
                $http.get(urls.FIND_PRODUCT_BY_SEQPRO + seqPro)
                    .then(
                        function (response) {
                        	$localStorage.findProduct=response.data;
                        	 deferred.resolve(response);
                        },
                        function (errResponse) {
                        	  deferred.reject(errResponse);
                        }
                    );
                return $localStorage.findProduct;
            }
            
            function addProduct(product) {
                var deferred = $q.defer();
                $http.post(urls.ADD_PRODUCT, product)
                    .then(
                        function (response) {
                        	loadAllProducts();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                           deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
            function updateProduct(product) {
                var deferred = $q.defer();
                $http.post(urls.UPDATE_PRODUCT,product)
                    .then(
                        function (response) {
                        	loadAllProducts();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
        }
    ]);