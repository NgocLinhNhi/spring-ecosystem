package heavensky.project.spring.boot.angurlarjs.service;

import heavensky.project.spring.boot.angurlarjs.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> loadAllProduct();

    Product findProductById(Long seqPro);

    Boolean addNewProduct(Product pro);

    long selectMaxSeqProduct();

    void deleteProduct(Long seqPro) throws Exception;

    Boolean changeStatus(Long seqPro) throws Exception;

    void updateProduct(Product pro) throws Exception;
}
