package heavensky.project.spring.boot.angurlarjs.repository;

import heavensky.project.spring.boot.angurlarjs.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import static heavensky.project.spring.boot.angurlarjs.sql.SqlQuery.LOGIN;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query(LOGIN)
    User loginUser(String loginName, String password);

}
