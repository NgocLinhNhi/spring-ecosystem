package heavensky.project.spring.boot.angurlarjs.repository;

import heavensky.project.spring.boot.angurlarjs.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

import static heavensky.project.spring.boot.angurlarjs.sql.SqlQuery.*;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(LOAD_ALL_PRODUCT)
    List<Product> loadAllProduct();

    @Query(FIND_PRODUCT_BY_ID)
    Product findProductById(@Param("seqPro") Long seqPro);

    @Query(value = SELECT_MAX_SEQ, nativeQuery = true)
    long selectMaxSeqProduct();

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(UPDATE_PRODUCT)
    int updateStatusProduct(Long id);
}
