package heavensky.project.spring.boot.angurlarjs.serviceimpl;

import heavensky.project.spring.boot.angurlarjs.entity.User;
import heavensky.project.spring.boot.angurlarjs.repository.UserRepository;
import heavensky.project.spring.boot.angurlarjs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User loginUser(User us) {
        return userRepository.loginUser(us.getLoginName(), us.getPassword());
    }

}