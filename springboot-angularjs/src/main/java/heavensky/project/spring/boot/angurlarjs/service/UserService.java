package heavensky.project.spring.boot.angurlarjs.service;

import heavensky.project.spring.boot.angurlarjs.entity.User;

public interface UserService {
    User loginUser(User us);
}
