package heavensky.project.spring.boot.angurlarjs.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import heavensky.project.spring.boot.angurlarjs.entity.Product;
import heavensky.project.spring.boot.angurlarjs.message.Response;
import heavensky.project.spring.boot.angurlarjs.serviceimpl.ProductServiceImpl;
import heavensky.project.spring.boot.angurlarjs.utils.Constant;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/product")
public class ProductController {
    @Autowired
    ProductServiceImpl productServiceImpl;

    private static Logger logger = Logger.getLogger(ProductController.class);

    @GetMapping(value = "/loadAllProduct")
    public ResponseEntity<List<Product>> loadAllProduct() {
        List<Product> loadAllProduct = new ArrayList<>();
        try {
            // load all thì category nhưng ép lên kiểu Json thì nó không cho .
            loadAllProduct = productServiceImpl.loadAllProduct();
        } catch (Exception e) {
            logger.error(Constant.EXCEPTION, e);
        }
        return new ResponseEntity<>(loadAllProduct, HttpStatus.OK);
    }

    // trả về dạng Json - đây xử lý = thư viện của jackson
    @GetMapping(value = "/loadAllProductJson")
    public String loadAllProductJson() {
        String result = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<Product> loadAllProduct = productServiceImpl.loadAllProduct();
            result = objectMapper.writeValueAsString(loadAllProduct);
        } catch (Exception e) {
            logger.error(Constant.EXCEPTION, e);
        }
        return result;
    }

    // sử dụng trong trường hợp update hay view = modal tại trang hiện tại
    @GetMapping(value = "/findProductBySeq/{seqPro}")
    public ResponseEntity<Product> getProductBySeqPro(@PathVariable long seqPro) {
        Product proFind = productServiceImpl.findProductById(seqPro);
        return new ResponseEntity<>(proFind, HttpStatus.OK);
    }

    @PostMapping(value = "/addNewProduct", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Response> addNewProduct(@RequestBody @Validated Product pro) {
        try {
            productServiceImpl.addNewProduct(pro);
        } catch (Exception e) {
            logger.error("Exception", e);
            return new ResponseEntity<>(new Response(
                    Constant.RESPONSE.EXCEPTION_STATUS,
                    Constant.RESPONSE.EXCEPTION_CODE,
                    Constant.RESPONSE.EXCEPTION_MESSAGE),
                    HttpStatus.EXPECTATION_FAILED);
        }
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.SUCCESS_STATUS,
                Constant.RESPONSE.SUCCESS_CODE,
                Constant.RESPONSE.SUCCESS_MESSAGE),
                HttpStatus.OK);
    }

    @PostMapping(value = "/deleteProduct/{seqPro}")
    public ResponseEntity<Response> deleteProduct(@PathVariable long seqPro) {
        try {
            productServiceImpl.deleteProduct(seqPro);
        } catch (Exception e) {
            logger.error(Constant.EXCEPTION, e);
            return new ResponseEntity<>(new Response(
                    Constant.RESPONSE.EXCEPTION_STATUS,
                    Constant.RESPONSE.EXCEPTION_CODE,
                    Constant.RESPONSE.EXIST_MESSAGE),
                    HttpStatus.EXPECTATION_FAILED);
        }
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.SUCCESS_STATUS,
                Constant.RESPONSE.SUCCESS_CODE,
                Constant.RESPONSE.SUCCESS_MESSAGE),
                HttpStatus.CREATED);
    }

    @PostMapping(value = "/updateProduct")
    public ResponseEntity<Response> updateProduct(@RequestBody Product pro) {
        try {
            productServiceImpl.updateProduct(pro);
        } catch (Exception e) {
            logger.error(Constant.EXCEPTION, e);
            return new ResponseEntity<>(new Response(
                    Constant.RESPONSE.EXCEPTION_STATUS,
                    Constant.RESPONSE.EXCEPTION_CODE,
                    Constant.RESPONSE.EXIST_MESSAGE),
                    HttpStatus.EXPECTATION_FAILED);
        }
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.SUCCESS_STATUS,
                Constant.RESPONSE.SUCCESS_CODE,
                Constant.RESPONSE.SUCCESS_MESSAGE),
                HttpStatus.OK);
    }
}
