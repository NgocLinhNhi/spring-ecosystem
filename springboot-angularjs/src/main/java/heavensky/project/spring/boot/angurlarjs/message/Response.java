package heavensky.project.spring.boot.angurlarjs.message;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {
	private int responseStatus;
	private String responseCode;
	private String message;

	public Response(int responseStatus, String responseCode, String message) {
		super();
		this.responseStatus = responseStatus;
		this.responseCode = responseCode;
		this.message = message;
	}

}
