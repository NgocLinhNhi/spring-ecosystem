package com.heavensky.springboot.mysql.mybatis.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class ProductView implements Serializable {
    private static final long serialVersionUID = -5538416125326898258L;

    private Long seqPro;
    private String categoryId;
    private String productName;
    private BigDecimal price;
    private String imageProduct;
    private BigDecimal numberSales;
    private BigDecimal guarantee;
    private Date createDate;
    private Date updateDate;
    private Long sysStatus;

    public ProductView() {
        super();
    }

    public ProductView(Long seqPro, String categoryId, String productName, BigDecimal price, String imageProduct,
                       BigDecimal numberSales, BigDecimal guarantee, Date createDate, Date updateDate, Long sysStatus) {
        super();
        this.seqPro = seqPro;
        this.categoryId = categoryId;
        this.productName = productName;
        this.price = price;
        this.imageProduct = imageProduct;
        this.numberSales = numberSales;
        this.guarantee = guarantee;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.sysStatus = sysStatus;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}