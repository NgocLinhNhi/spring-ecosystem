package com.heavensky.springboot.mysql.mybatis.repository;

import com.heavensky.springboot.mysql.mybatis.domain.ProductView;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ProductDao {
    List<ProductView> loadAllProduct();
}
