package com.heavensky.springboot.mysql.mybatis.service;

import com.heavensky.springboot.mysql.mybatis.domain.ProductView;

import java.util.List;

public interface ProductService {
	List<ProductView> loadAllProduct();
}
