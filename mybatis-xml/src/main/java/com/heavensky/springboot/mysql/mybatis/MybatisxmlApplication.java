package com.heavensky.springboot.mysql.mybatis;

import com.heavensky.springboot.mysql.mybatis.domain.ProductView;
import com.heavensky.springboot.mysql.mybatis.serviceimpl.ProductServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.List;

@SpringBootApplication
public class MybatisxmlApplication {
    private static Logger logger = LoggerFactory.getLogger(MybatisxmlApplication.class);

    //Test = Application Context không cần import lib for SpringTest để gọi service
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(MybatisxmlApplication.class, args);
        loadProductTest(applicationContext);
    }

    private static void loadProductTest(ApplicationContext applicationContext) {
        ProductServiceImpl productServiceImpl = applicationContext.getBean(ProductServiceImpl.class);
        findAllProducts(productServiceImpl);
    }

    public static void findAllProducts(ProductServiceImpl productServiceImpl) {
        List<ProductView> productViews = productServiceImpl.loadAllProduct();

        logger.info("================= LOAD BY XML FILE ================= ");
        for (ProductView product : productViews) {
            logger.info("Product Name : {} Category Name : {}", product.getProductName(), product.getCategoryId());
        }
    }

}
