package com.heavensky.springboot.mysql.mybatis.serviceimpl;

import com.heavensky.springboot.mysql.mybatis.domain.ProductView;
import com.heavensky.springboot.mysql.mybatis.repository.ProductDao;
import com.heavensky.springboot.mysql.mybatis.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    private Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    protected ProductDao productDao;

    @Value("${value}")
    private String value;

    @Override
    public List<ProductView> loadAllProduct() {
        logger.info("Value From file Application Properties ======= " + value);
        return productDao.loadAllProduct();
    }
}
