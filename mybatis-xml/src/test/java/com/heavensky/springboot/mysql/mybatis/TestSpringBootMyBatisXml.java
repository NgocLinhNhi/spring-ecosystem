package com.heavensky.springboot.mysql.mybatis;

import com.heavensky.springboot.mysql.mybatis.domain.ProductView;
import com.heavensky.springboot.mysql.mybatis.serviceimpl.ProductServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestSpringBootMyBatisXml {
    private Logger logger = LoggerFactory.getLogger(TestSpringBootMyBatisXml.class);

    @Autowired
    private ProductServiceImpl productServiceImpl;

    @Test
    public void findAllProducts() {
        List<ProductView> productViews = productServiceImpl.loadAllProduct();

        logger.info("================= LOAD BY XML FILE ================= ");
        for (ProductView product : productViews) {
            logger.info("Product Name : {} Category Name : {}", product.getProductName(), product.getCategoryId());
        }
    }

}
