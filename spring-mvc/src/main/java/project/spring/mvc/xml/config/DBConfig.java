package project.spring.mvc.xml.config;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

public class DBConfig {

    private static JdbcTemplate jdbcTemplate;
    private static NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private static DBConfig INSTANCE;

    public static DBConfig getInstance() {
        if (INSTANCE == null) INSTANCE = new DBConfig();
        return INSTANCE;
    }

    public JdbcTemplate getConnection() {
        if (jdbcTemplate == null)
            jdbcTemplate = new JdbcTemplate(getDataSource());

        return jdbcTemplate;
    }

    //Cách 1 Lấy dataSource = config DataSource = java or annotation @Bean
    //DataSourceConfig.getInstance().dataSource()
    public NamedParameterJdbcTemplate getNamedConnection() throws Exception {
        if (namedParameterJdbcTemplate == null)
            namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(DataSourceConfig.getInstance().dataSource());
        return namedParameterJdbcTemplate;
    }

    //Cách 2 : Lấy DataSource = ApplicationContext từ file xml.
    private static DataSource getDataSource() {
        return (DataSource) SpringApplicationContext.getBean("dataSource");
    }
}
