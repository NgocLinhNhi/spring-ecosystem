package project.spring.mvc.xml.object_mapper;

import org.springframework.jdbc.core.RowMapper;
import project.spring.mvc.xml.entity.Customer;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerMapper implements RowMapper {

    public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
        Customer customer = new Customer();
        customer.setName(rs.getString("name"));
        customer.setAddress(rs.getString("address"));
        customer.setId(rs.getInt("id"));
        return customer;
    }
}