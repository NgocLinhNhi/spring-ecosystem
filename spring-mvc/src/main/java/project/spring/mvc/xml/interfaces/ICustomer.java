package project.spring.mvc.xml.interfaces;

import project.spring.mvc.xml.entity.Customer;

import java.util.List;

public interface ICustomer {
    void save(Customer customer);

    void update(Customer customer);

    void delete(int customerId);

    Customer findById(int customerId);

    List<Customer> findAllCustomer();
}
