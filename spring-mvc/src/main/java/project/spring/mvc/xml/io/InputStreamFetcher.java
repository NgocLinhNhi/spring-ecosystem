package project.spring.mvc.xml.io;

import java.io.InputStream;

public interface InputStreamFetcher {

    InputStream getInputStream(String filePath);

}
