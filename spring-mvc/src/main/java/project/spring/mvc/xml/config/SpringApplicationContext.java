package project.spring.mvc.xml.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringApplicationContext implements ApplicationContextAware {

    private static ApplicationContext CONTEXT;
    //Config động kiểu này không phải truyền file kiểu để lấy ra file
    // ApplicationContext applicationContext = new ClassPathXmlApplicationContext("../Spring-config-servlet.xml")

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        CONTEXT = applicationContext;
    }

    public static Object getBean(String beanName) {
        return CONTEXT.getBean(beanName);
    }

}

