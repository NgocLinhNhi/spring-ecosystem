package project.spring.mvc.xml.config;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import project.spring.mvc.xml.io.InputStreams;

import javax.sql.DataSource;
import java.util.Properties;

class DataSourceConfig {

    private static DataSourceConfig INSTANCE;

    static DataSourceConfig getInstance() {
        if (INSTANCE == null) INSTANCE = new DataSourceConfig();
        return INSTANCE;
    }

    //Lấy DataSource = cách 1
    DataSource dataSource() throws Exception {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(getInstance().loadProperties().getProperty("jdbc.driverClassName"));
        dataSource.setUrl(getInstance().loadProperties().getProperty("jdbc.url"));
        dataSource.setUsername(getInstance().loadProperties().getProperty("jdbc.username"));
        dataSource.setPassword(getInstance().loadProperties().getProperty("jdbc.password"));
        return dataSource;
    }

    //load File Properties
    private Properties loadProperties() throws Exception {
        Properties properties = new Properties();
        properties.load(InputStreams.getInputStream(getPropertiesFile()));
        return properties;
    }

    private String getPropertiesFile() {
        String file = System.getProperty("application.properties");
        if (file == null)
            file = "application.properties";
        return file;
    }
}
