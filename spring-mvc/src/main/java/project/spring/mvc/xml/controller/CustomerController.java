package project.spring.mvc.xml.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import project.spring.mvc.xml.entity.Customer;
import project.spring.mvc.xml.service.CustomerServiceImpl;

@Controller
public class CustomerController {

    @Autowired
    CustomerServiceImpl customerService;

    @RequestMapping(value = {"/", "/customer-list"})
    public String listCustomer(Model model) {
        model.addAttribute("listCustomer", customerService.findAllCustomer());
        return "customer-list";
    }

    @RequestMapping("/customer-save")
    public String insertCustomer(Model model) {
        model.addAttribute("customer", new Customer());
        return "customer-save";
    }

    @RequestMapping("/customer-view/{id}")
    public String viewCustomer(@PathVariable int id, Model model) {
        Customer customer = customerService.findById(id);
        model.addAttribute("customer", customer);
        return "customer-view";
    }

    @RequestMapping("/customer-update/{id}")
    public String updateCustomer(@PathVariable int id, Model model) {
        Customer customer = customerService.findById(id);
        model.addAttribute("customer", customer);
        return "customer-update";
    }

    @PostMapping("/saveCustomer")
    public String doSaveCustomer(@ModelAttribute("Customer") Customer customer, Model model) {
        customerService.save(customer);
        model.addAttribute("listCustomer", customerService.findAllCustomer());
        //để thế này khi load lại trang load all nó tự insert vì đang có URI đến controller save -> bug phát sinh
        //return "customer-list";
        return "redirect:customer-list";
    }

    @RequestMapping("/updateCustomer")
    public String doUpdateCustomer(@ModelAttribute("Customer") Customer customer, Model model) {
        customerService.update(customer);
        model.addAttribute("listCustomer", customerService.findAllCustomer());
        return "customer-list";
    }

    @RequestMapping("/customerDelete/{id}")
    public String doDeleteCustomer(@PathVariable int id, Model model) {
        customerService.delete(id);
        model.addAttribute("listCustomer", customerService.findAllCustomer());
        return "customer-list";
    }
}
