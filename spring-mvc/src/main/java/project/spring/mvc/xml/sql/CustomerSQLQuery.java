package project.spring.mvc.xml.sql;

public class CustomerSQLQuery {
    public static final String FIND_ALL_CUSTOMER = "SELECT * FROM CUSTOMER";
    public static final String FIND_CUSTOMER_BY_ID = "SELECT * FROM CUSTOMER WHERE ID = ?";
    public static final String DELETE_CUSTOMER = "DELETE FROM CUSTOMER WHERE ID = ?";

    public static StringBuilder InsertCustomer() {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO  CUSTOMER            ")
                .append("    (                       ")
                .append("       ID,                  ")
                .append("       NAME,                ")
                .append("       ADDRESS              ")
                .append("    )                       ")
                .append("    VALUES(?,?,?)           ");
        return sql;
    }

    public static StringBuilder updateCustomer() {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE  CUSTOMER   SET         ")
                .append("     NAME = ?   ,         ")
                .append("     ADDRESS = ?          ")
                .append(" WHERE                    ")
                .append("     ID = ?               ");
        return sql;

    }
}
