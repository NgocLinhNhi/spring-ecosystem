package project.spring.mvc.xml.service;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import project.spring.mvc.xml.config.DBConfig;
import project.spring.mvc.xml.entity.Customer;
import project.spring.mvc.xml.interfaces.ICustomer;
import project.spring.mvc.xml.object_mapper.CustomerMapper;

import java.util.List;

import static project.spring.mvc.xml.sql.CustomerSQLQuery.*;

@Service
public class CustomerServiceImpl implements ICustomer {

    @Override
    public void save(Customer customer) {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        jdbcTemplate.update(
                InsertCustomer().toString(),
                customer.getId(),
                customer.getName(),
                customer.getAddress());
    }

    @Override
    public void update(Customer customer) {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        jdbcTemplate.update(
                updateCustomer().toString(),
                customer.getName(),
                customer.getAddress(),
                customer.getId());
    }

    @Override
    public void delete(int customerId) {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        Object[] custId = new Object[]{customerId};
        jdbcTemplate.update(DELETE_CUSTOMER, custId);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Customer findById(int customerId) {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        Object[] custId = new Object[]{customerId};
        return (Customer) jdbcTemplate.queryForObject(
                FIND_CUSTOMER_BY_ID,
                custId,
                new CustomerMapper());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Customer> findAllCustomer() {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(FIND_ALL_CUSTOMER, new CustomerMapper());
    }
}
