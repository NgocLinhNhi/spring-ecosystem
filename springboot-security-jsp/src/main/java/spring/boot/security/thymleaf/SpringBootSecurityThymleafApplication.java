package spring.boot.security.thymleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSecurityThymleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSecurityThymleafApplication.class, args);
    }

}
