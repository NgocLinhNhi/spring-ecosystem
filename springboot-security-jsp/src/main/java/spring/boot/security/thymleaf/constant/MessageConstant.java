package spring.boot.security.thymleaf.constant;

public class MessageConstant {

    public static final String MESSAGE = "<br> You do not have permission to access this page!";
    public static final String ATTRIBUTE = "message";
}
