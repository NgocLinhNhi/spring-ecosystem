package spring.boot.security.thymleaf.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import spring.boot.security.thymleaf.data.CreateDataTest;
import spring.boot.security.thymleaf.repository.RoleRepository;
import spring.boot.security.thymleaf.repository.UserRepository;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent arg0) {
        // Khi App khởi động tự insert vào DB bảng Roles với các role bên dưới
        CreateDataTest.getInstance(userRepository, roleRepository, passwordEncoder).createData();
    }

}
