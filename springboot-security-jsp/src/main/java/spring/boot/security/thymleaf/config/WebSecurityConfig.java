package spring.boot.security.thymleaf.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Qualifier("userDetailsServiceImpl")
    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/register").permitAll()
                //role MEMBER - ADMIN là mặc định của config data trong Database = đổi = die
                //tất cả các URI còn lại đòi hỏi role MEMBER
                .antMatchers("/").hasRole("MEMBER")
                //1 URI cho nhiều role đăng nhập vào nếu vào role ko đc phép nó tự redirect về trang login
                .antMatchers("/admin").hasRole("ADMIN")
                .antMatchers("/admin").hasRole("SIDA")
                .and()
                .formLogin()
                //.loginProcessingUrl("/userLogin")//set up 1 trang login khác dung post method
                .loginPage("/login")//trang login mặc định có thể thay đổi
                .usernameParameter("email")//quy đinh login = userName/Email
                .passwordParameter("password")//Quy định cột password có hay không
                .defaultSuccessUrl("/")//login success gọi đên URI index
                //.defaultSuccessUrl("/loginAccess",true)//set up đến 1 URI khác sau khi login success
                .failureUrl("/login?error")
                .and()
                .exceptionHandling()
                .accessDeniedPage("/403");
    }

}
