package spring.boot.security.thymleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

import static spring.boot.security.thymleaf.constant.MessageConstant.ATTRIBUTE;
import static spring.boot.security.thymleaf.constant.MessageConstant.MESSAGE;

@Controller
public class MainController {

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/admin")
    public String admin() {
        return "admin";
    }


    @GetMapping(value = "/403")
    public String accessDenied(Model model, Principal principal) {
        //có user nhưng không có quyền
        if (principal != null) {
            model.addAttribute(ATTRIBUTE, "Hi " + principal.getName() + MESSAGE);
            //Trường hợp chưa cả login
        } else {
            model.addAttribute(ATTRIBUTE, MESSAGE);
        }
        return "403";
    }

    @GetMapping("/login")
    public String getLogin() {
        return "login";
    }

    @GetMapping("/loginAccess")
    public String loginAccess() {
        return "home";
    }

}
