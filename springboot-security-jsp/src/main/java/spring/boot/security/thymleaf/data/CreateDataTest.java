package spring.boot.security.thymleaf.data;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import spring.boot.security.thymleaf.entity.Role;
import spring.boot.security.thymleaf.entity.User;
import spring.boot.security.thymleaf.repository.RoleRepository;
import spring.boot.security.thymleaf.repository.UserRepository;

import java.util.HashSet;

public class CreateDataTest {

    private static CreateDataTest INSTANCE;

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    public static CreateDataTest getInstance(UserRepository userRepository,
                                             RoleRepository roleRepository,
                                             PasswordEncoder passwordEncoder) {
        if (INSTANCE == null) INSTANCE = new CreateDataTest(
                userRepository,
                roleRepository,
                passwordEncoder);
        return INSTANCE;
    }

    private CreateDataTest(UserRepository userRepository,
                           RoleRepository roleRepository,
                           PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void createData() {
        createAdminRole();
        createMemberRole();
        createAdminUser();
        createUserAccount();
    }

    private void createAdminRole() {
        if (roleRepository.findByName("ROLE_ADMIN") == null) {
            roleRepository.save(new Role("ROLE_ADMIN"));
        }
    }

    private void createMemberRole() {
        if (roleRepository.findByName("ROLE_MEMBER") == null) {
            roleRepository.save(new Role("ROLE_MEMBER"));
        }
    }

    private void createAdminUser() {
        // Admin account
        if (userRepository.findByEmail("admin@gmail.com") == null) {
            User admin = new User();
            admin.setEmail("admin@gmail.com");
            admin.setPassword(passwordEncoder.encode("123456"));
            HashSet<Role> roles = new HashSet<>();

            roles.add(roleRepository.findByName("ROLE_ADMIN"));
            roles.add(roleRepository.findByName("ROLE_MEMBER"));
            admin.setRoles(roles);
            userRepository.save(admin);
        }
    }

    private void createUserAccount() {
        // Member account
        if (userRepository.findByEmail("member@gmail.com") == null) {
            User user = new User();
            user.setEmail("member@gmail.com");
            user.setPassword(passwordEncoder.encode("123456"));
            HashSet<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByName("ROLE_MEMBER"));
            user.setRoles(roles);
            userRepository.save(user);
        }
    }
}
