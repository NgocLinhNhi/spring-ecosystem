package heavensky.project.spring.ecosystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEcosystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringEcosystemApplication.class, args);
    }

}
