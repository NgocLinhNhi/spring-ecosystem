package heavensky.spring.boot.security.jsp.repository;

import heavensky.spring.boot.security.jsp.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role findByName(String name);

}
