package heavensky.spring.boot.security.jsp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

import static heavensky.spring.boot.security.jsp.constant.MessageConstant.ATTRIBUTE;
import static heavensky.spring.boot.security.jsp.constant.MessageConstant.MESSAGE;

@Controller
public class MainController {

    @GetMapping("/index")
    public String index() {
        return "pages/index";
    }

    @GetMapping("/admin")
    public String admin() {
        return "pages/admin";
    }

    @GetMapping(value = "/403")
    public String accessDenied(Model model, Principal principal) {
        if (principal != null) {
            model.addAttribute(ATTRIBUTE, "Hi " + principal.getName() + MESSAGE);
        } else {
            model.addAttribute(ATTRIBUTE, MESSAGE);
        }
        return "pages/403";
    }

    @GetMapping("/")
    public String getLogin() {
        return "loginPage";
    }

    @RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
    public String logoutSuccessfulPage(Model model) {
        model.addAttribute("title", "Logout");
        return "pages/logoutSuccessfulPage";
    }

}
