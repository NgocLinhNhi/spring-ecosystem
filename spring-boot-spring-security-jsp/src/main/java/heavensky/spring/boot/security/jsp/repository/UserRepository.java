package heavensky.spring.boot.security.jsp.repository;

import heavensky.spring.boot.security.jsp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByEmail(String email);

}
