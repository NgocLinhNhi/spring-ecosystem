package springboot.project.ecosystem.jpa.sql;

public class SqlRepositoryQuery {

    public static final String LOGIN_USER = "select us from User us where us.loginName = ?1 and us.password = ?2 and isDelete=1";
    public static final String GET_ALL_USER = "SELECT us.* FROM USER_CUSTOMER us ";
    public static final String LOGIN_USER_2 = "select us.* from USER_CUSTOMER us where us.LOGIN_NAME = ?1 and us.PASSWORD = ?2";
    public static final String SELECT_MAX_USER = "select max(SEQ_USER) from USER_CUSTOMER";
    public static final String CHECK_EXIST_ACCOUNT = "select us from User us where us.loginName=:loginName and us.isDelete=1";
}
