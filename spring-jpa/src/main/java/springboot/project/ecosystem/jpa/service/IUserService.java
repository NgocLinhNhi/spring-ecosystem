package springboot.project.ecosystem.jpa.service;

import org.springframework.stereotype.Service;
import springboot.project.ecosystem.jpa.entity.User;

import java.util.List;

@Service
public interface IUserService {

    User loginUser(User us);

    Boolean registerMember(User us);

    long selectMaxSeq();

    Boolean checkExistLoginName(User us);

    List<User> getAllUser();
}
