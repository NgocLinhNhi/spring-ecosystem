package springboot.project.ecosystem.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import springboot.project.ecosystem.jpa.entity.User;
import springboot.project.ecosystem.jpa.service_impl.UserServiceImpl;

import java.util.List;

@SpringBootApplication
public class SpringjpaApplication {
    private static Logger logger = LoggerFactory.getLogger(SpringjpaApplication.class);

    //update Test = Dùng ApplicationContext thay cho lib SpringBootTest
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(SpringjpaApplication.class, args);
        loadAllUser(applicationContext);
    }

    private static void loadAllUser(ApplicationContext applicationContext) {
        UserServiceImpl userService = applicationContext.getBean(UserServiceImpl.class);
        getAllCustomer(userService);
    }

    private static void getAllCustomer(UserServiceImpl userService) {
        List<User> allUser = userService.getAllUser();
        for (User user : allUser) {
            logger.info("Customer Name : {}", user.getLoginName());
            logger.info("CreateDate : {}", user.getCreateDate());
        }
    }


}
