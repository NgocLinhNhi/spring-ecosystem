package springboot.project.ecosystem.jpa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import springboot.project.ecosystem.jpa.entity.User;

import java.util.List;

import static springboot.project.ecosystem.jpa.sql.SqlRepositoryQuery.*;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    //nativeQuery = true => query với table trong DB
    //không set nativeQuery = true => query với Table là tên class trong Java( kiểu HQL)

    @Query(value = LOGIN_USER)
    User loginUser(String loginName, String password);

    @Query(value = GET_ALL_USER, nativeQuery = true)
    List<User> getAllUser();

    @Query(value = LOGIN_USER_2, nativeQuery = true)
    User loginUserOther(String loginName, String password);

    @Query(value = SELECT_MAX_USER, nativeQuery = true)
    int selectMaxUser();

    @Query(value = CHECK_EXIST_ACCOUNT)
    User checkExistLoginName(@Param("loginName") String loginName);
}
