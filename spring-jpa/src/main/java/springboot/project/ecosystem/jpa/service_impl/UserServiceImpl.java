package springboot.project.ecosystem.jpa.service_impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.project.ecosystem.jpa.entity.User;
import springboot.project.ecosystem.jpa.repository.UserRepository;
import springboot.project.ecosystem.jpa.service.IUserService;

import java.util.Date;
import java.util.List;

@Transactional
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User loginUser(User us) {
        return userRepository.loginUser(us.getLoginName(), us.getPassword());
    }

    @Override
    public Boolean registerMember(User user) {
        long selectMaxUser = userRepository.selectMaxUser();
        createUser(selectMaxUser, user);
        User saveAndFlush = userRepository.save(user);
        return saveAndFlush != null;
    }

    @Override
    public long selectMaxSeq() {
        return userRepository.selectMaxUser();
    }

    @Override
    public Boolean checkExistLoginName(User us) {
        User result = userRepository.checkExistLoginName(us.getLoginName());
        return result != null;
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.getAllUser();
    }

    private void createUser(long selectMaxUser, User us) {
        long seqNo = selectMaxUser + 1;
        us.setSeqUser(seqNo);
        us.setRoleUser(1);
        us.setIsDelete(1);
        Date date = new Date();
        us.setCreateDate(date);
    }

}