package springboot.project.ecosystem.jpa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.project.ecosystem.jpa.entity.User;
import springboot.project.ecosystem.jpa.service_impl.UserServiceImpl;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
// không có config này không lấy được data DB cho class Test đâu
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//Nếu gọi từ lớp Service thì phải có ComponentScan => không thì nó không load được bean TestUserRepository
@ComponentScan("springboot.project.ecosystem.jpa")
public class TestUserRepository {
    private Logger logger = LoggerFactory.getLogger(TestUserRepository.class);

    //Các Entity class phải để trong cùng module nó mới load được
    @Autowired
    UserServiceImpl userService;

    @Test
    public void getAllCustomer() {
        List<User> allUser = userService.getAllUser();
        for (User user : allUser) {
            logger.info("Customer Name : {}", user.getLoginName());
            logger.info("CreateDate : {}", user.getCreateDate());
        }
    }

    @Test
    public void testLogin() {
        User loginUser = userService.loginUser(createUser());
        logger.info("Login with customer name : {}", loginUser != null ? "SUCCESSFULLY" : "FAILED");
    }

    @Test
    public void testCheckExistLoginName() {
        Boolean result = userService.checkExistLoginName(createUser());
        if (result != null) {
            logger.info("tên đăng nhập đã tồn tại");
        } else {
            logger.info("Tên đăng nhập hợp lệ");
        }
    }

    private User createUser() {
        User user = new User();
        user.setUserName("viet");
        user.setPassword("16051987");
        return user;
    }
}
