<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
	<head>
		<title>Spring Security Example</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	</head>

	<body>
		<div class="container" style="margin: 50px">
			<h3>Spring Security Login Example</h3>
			<label class="col-sm-3"></label><label class="col-sm-9" id="err-ms" style="color: red"></label>

			<form name="formLogin " action="/j_spring_security_check" method="POST">
				<div class="form-group">
					<label for="email">UserName day: <input type="text"
						class="form-control" id="email" name="email"></label>
				</div>

				<div class="form-group">
					<label for="pwd">Password day:</label> <input type="password"
						style="width: 220px;" class="form-control" id="pwd" name="password">
				</div>

				<button type="button" onclick="doLogin()" id="btn-login"
					class="btn btn-info">Login</button>

			</form>
		</div>

		<script type="text/javascript">
		//thế này mới submit form mà giữ được URL đúng SpringMVCSecurityAnnotationMySql với method POST được bó tay -,-
		//-ko se bi localhost:8080/login -> loi 404 ->Spring MVC kha chuoi
			function doLogin() {
				var email = $("#email").val();
				var password = $("#pwd").val();

				$.ajax({
					type : 'post',
					method : 'post',
					url : 'j_spring_security_check',
					data : {
						email : email,
						password : password
					},
					success : function(data) {
						location.href = 'index';
					},
					error : function(data){
						//hiển thị message login fail = ajjax ko dùng mặc định của spring Security
						 $("#err-ms").html('Wrong User Name or password')
					}
				});
			}
		</script>

	</body>
</html>