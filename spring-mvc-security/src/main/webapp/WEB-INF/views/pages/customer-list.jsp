<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<!--  khong co language = java thi ko hien thi ko dung jstl -->
	<head>
	<title>Helo Spring MVC + JDBC</title>
	<style>
	table, th, td {
		border: 1px solid black;
	}
	td {
		padding-right: 30px;
	}
	</style>
	</head>
	<body>
		<h1>List Customer:</h1>
		<br />
		<br />

		<table>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Address</th>
				<th>View</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
			<c:if test="${not empty listCustomer}">
				<c:forEach var="customer" items="${listCustomer}">
					<tr style="border: 1px black solid">
						<td>${customer.id}</td>
						<td>${customer.name}</td>
						<td>${customer.address}</td>
						<td> <a href="${urlView}/${customer.id}">View</a></td>
						<td> <a href="${urlUpdate}/${customer.id}">Edit</a></td>
						<td> <a href="${urlDelete}/${customer.id}">Delete</a></td>
					</tr>
				</c:forEach>
			</c:if>
		</table>

	</body>
</html>