package project.spring.mvc.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
public class MainController {

    @Autowired
    UserDetailsService userDetailsService;

    @GetMapping("/index")
    public String index() {
        return "pages/index";
    }

    @GetMapping("/admin")
    public String admin() {
        return "pages/admin";
    }

    @GetMapping(value = "/403")
    public ModelAndView accessDenied(ModelMap modelMap, Principal principal) {
        ModelAndView model = new ModelAndView("pages/403");
        if (principal != null) {
            //do bản jsp cũ 1.2 nên phải co them <%@ page isELIgnored="false" %> ở trang jsp
            modelMap.addAttribute("msg", "Hi " + principal.getName() + "<br> You do not have permission to access this page!");
        } else {
            modelMap.addAttribute("msg", "You do not have permission to access this page!");
        }
        return model;
    }

    @GetMapping("/")
    public String getLogin() {
        return "loginPage";
    }

    @RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
    public String logoutSuccessfulPage(Model model) {
        model.addAttribute("title", "Logout");
        return "pages/logoutSuccessfulPage";
    }

}
