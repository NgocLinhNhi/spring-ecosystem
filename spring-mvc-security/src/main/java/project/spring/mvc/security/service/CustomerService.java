package project.spring.mvc.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import project.spring.mvc.security.dao.CustomerRepositoy;
import project.spring.mvc.security.entity.Customer;
import project.spring.mvc.security.interfaces.ICustomerService;

import java.util.List;

@Service("customerService")
public class CustomerService implements ICustomerService {

    @Autowired
    private CustomerRepositoy customerDAO;

    @Override
    public List<Customer> findAll() {
        return customerDAO.findAll();
    }
}
