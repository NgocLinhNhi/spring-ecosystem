package project.spring.mvc.security.security_config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
//ko co quet componentScan loi Error creating bean with name 'securityConfiguration'
@ComponentScan("project.spring.mvc.security")
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.csrf().disable();
		http.authorizeRequests().antMatchers("/static/**","/", "/welcome", "/login", "/logout").permitAll();

		http.authorizeRequests().antMatchers("/index","/customer-list").hasAnyRole("MEMBER", "ADMIN");
		http.authorizeRequests().antMatchers("/admin").hasRole("ADMIN");
		http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");

		http.authorizeRequests()
			.and()
				.formLogin()
					.loginPage("/login")
					.loginProcessingUrl("/j_spring_security_check")
					.defaultSuccessUrl("/index",true)
					//.failureUrl("/?error=true")//ko dung error cua spring security ma dung message cua ajax tra ve
					.usernameParameter("email")
					.passwordParameter("password")
			.and()
				.logout()
					.logoutUrl("/logout")
					.logoutSuccessUrl("/logoutSuccessful")
					.deleteCookies("auth_code", "JSESSIONID")
					.invalidateHttpSession(true);
	}
}
