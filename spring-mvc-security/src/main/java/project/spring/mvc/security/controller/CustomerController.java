package project.spring.mvc.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import project.spring.mvc.security.service.CustomerService;

@Controller
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/customer-list")
    public String listCustomer(ModelMap model) {
        model.addAttribute("listCustomer", customerService.findAll());
        return "pages/customer-list";
    }

}
