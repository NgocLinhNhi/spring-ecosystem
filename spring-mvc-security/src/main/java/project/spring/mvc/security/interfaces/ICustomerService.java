package project.spring.mvc.security.interfaces;

import project.spring.mvc.security.entity.Customer;

import java.util.List;

public interface ICustomerService {
    List<Customer> findAll();
}
