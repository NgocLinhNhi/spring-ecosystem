package project.spring.mvc.security.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.spring.mvc.security.entity.Customer;

@Repository
public interface CustomerRepositoy extends JpaRepository<Customer, Integer> {

}
