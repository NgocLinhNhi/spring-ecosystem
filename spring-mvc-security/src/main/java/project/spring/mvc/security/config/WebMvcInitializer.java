package project.spring.mvc.security.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import project.spring.mvc.security.security_config.SecurityConfiguration;

public class WebMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    // class java này = thay cho org.springframework.web.servlet.DispatcherServlet trong web.xml
    // add config spring-security vao file web.xml
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{HibernateConfig.class, SecurityConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{ServletInitializer.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

}
